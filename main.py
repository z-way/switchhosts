#!/usr/bin/env python
# coding=utf-8

import sys
import cPickle as pickle

from PyQt4 import QtGui

class MainPanel(QtGui.QWidget):

    def __init__(self):
        super(MainPanel, self).__init__()
        self.initConfig()
        self.initUI()

    def initUI(self):
        # Left sidebar
        configList = QtGui.QListWidget()
        configList.setGeometry(0, 0, 100, 100)

        hostsConfig = self.getConfig()

        for env, hosts in hostsConfig.items():
            configList.addItem(env)

        # Main text
        configContent = QtGui.QTextEdit()

        # Footer
        saveBtn = QtGui.QPushButton('Save')
        cancelBtn = QtGui.QPushButton('Cancel')
        hBox = QtGui.QHBoxLayout()
        hBox.addStretch(1)
        hBox.addWidget(saveBtn)
        hBox.addWidget(cancelBtn)

        # Grids
        grid = QtGui.QGridLayout()
        grid.addWidget(configList, 0, 0)
        grid.addWidget(configContent, 0, 1)
        grid.addLayout(hBox, 1, 1)

        self.setLayout(grid)
        self.setGeometry(300, 300, 600, 480)
        self.setWindowTitle('HostsSwitcher')
        self.show()

    def initConfig(self):
        try:
            configs = self.getConfig()
        except EOFError:
            configFile = './hosts.conf'
            configValue = {
                'Default' : '''
127.0.0.1        localhost''',
                'Pre-release' : '''
192.168.1.242    yzone.igrow.cn
192.168.1.242    newstar.igrow.cn
192.168.1.242    auth.igrow.cn
192.168.1.242    api.igrow.cn''',
                'Test' : '''
192.168.1.241    yzone.igrow.cn
192.168.1.241    newstar.igrow.cn
192.168.1.241    auth.igrow.cn
192.168.1.241    api.igrow.cn''',
                'Local' : '''
127.0.0.1        yzone.igrow.cn
127.0.0.1        newstar.igrow.cn
192.168.1.241    auth.igrow.cn
192.168.1.241    api.igrow.cn''',
            }

            f = file(configFile, 'w')
            pickle.dump(configValue, f)
            f.close()

    def getConfig(self):
        configFile = './hosts.conf'

        f = file(configFile)
        return pickle.load(f)

def main():
    app = QtGui.QApplication(sys.argv)
    ex = MainPanel()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()